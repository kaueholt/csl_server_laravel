<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array('prefix' => 'v1'), function(){
    Route::get('/', function () {
        return response()->json(['message' => 'csl API', 'status' => 'Connected']);
    });

    //Route::resource('auth', 'UserAuthController');
    Route::post('/login', 'UserAuthController@login');

    // Rotas default
    Route::delete('/default/{table}/{id}', 'DefaultController@destroy');
    Route::get('/default/{table}', 'DefaultController@index');
    Route::get('/default/{table}/{id}', 'DefaultController@show');
    Route::post('/default/{table}', 'DefaultController@store');
    Route::put('/default/{table}/{id}', 'DefaultController@update');
    Route::get('/default/usuario/uid/{uid}', 'DefaultController@showUid');
    Route::get('/default/usuario/cpf/{cpf}', 'DefaultController@showCpf');
    Route::post('/push/mobile/{token}', 'FirebaseController@sendPushNotification');
    
    
    // Rotas customizadas
    // Route::get('/teste/{horas}', 'PedidoController@calculaDataPrevistaEntregaHorasCorridas');
    
});

Route::get('/', function () {
    return redirect('api');
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

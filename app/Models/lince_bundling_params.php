<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Column      |           Type           | Collation | Nullable | Default
// -----------------+--------------------------+-----------+----------+---------
//  id              | uuid                     |           | not null |
//  endpointwebhook | character varying(255)   |           |          |
//  username        | character varying(255)   |           |          |
//  password        | character varying(255)   |           |          |
//  token_login     | character varying(255)   |           |          |
//  token_sessao    | character varying(255)   |           |          |
//  token_validade  | timestamp with time zone |           |          |
//  parceiro_id     | uuid                     |           | not null |
//  created_at      | timestamp with time zone |           | not null |
//  updated_at      | timestamp with time zone |           | not null |

class lince_bundling_params extends Model
{
    //
}

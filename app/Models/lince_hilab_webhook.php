<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// Column    |          Type          | Collation | Nullable | Default
// -------------+------------------------+-----------+----------+---------
//  id          | uuid                   |           | not null |
//  parceiro_id | uuid                   |           | not null |
//  token       | text                   |           |          |
//  type        | character varying(80)  |           |          |
//  timestamp   | bigint                 |           |          |
//  patient     | json                   |           |          |
//  start_time  | bigint                 |           |          |
//  CNPJ        | character varying(20)  |           |          |
//  custom      | character varying(255) |           |          |

/**
 * Class lince_hilab_webhook
 *
 * @property int $id
 * @property string $parceiro_id
 * @property string $token
 * @property string $type
 * @property int $timestamp
 * @property string $patient
 * @property int $start_time
 * @property string $CNPJ
 * @property string $custom
 *
 * @package App\Models
 */


class lince_hilab_webhook extends Model
{
    protected $table = 'lince_hilab_webhook';
	public $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'timestamp' => 'int',
		'start_time' => 'int'
	];

	protected $fillable = [
		'parceiro_id',
		'token',
		'type',
		'timestamp',
		'patient',
		'start_time',
		'CNPJ',
		'custom'
	];
}


// class usuario extends Eloquent
// {
// }


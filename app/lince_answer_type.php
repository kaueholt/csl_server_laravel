<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Column   |           Type           | Collation | Nullable | Default
// ------------+--------------------------+-----------+----------+---------
//  id         | uuid                     |           | not null |
//  tipo       | character varying(255)   |           | not null |
//  created_at | timestamp with time zone |           | not null |
//  updated_at | timestamp with time zone |           | not null |
//  Indexes:
//     "lince_answer_type_pkey" PRIMARY KEY, btree (id)
//     "tipo_unique" UNIQUE CONSTRAINT, btree (tipo)

class lince_answer_type extends Model
{
    //
}

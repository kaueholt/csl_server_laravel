<?php

namespace App\Http\Controllers;

/*------------------------------------------------------------------------------------------------------------------------------*/

     /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Cleaner API",
     *      description="Cleaner Swagger Api - By 33 Robotics",
     *      @OA\Contact(
     *          email="marcio.sergio@admsistema.com.br"
     *      ),
     *     @OA\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */
      
     /**
     * @OA\Delete(
     *     path="/api/v1/default/{table}/{ID}",
     *     summary="Deleta o registro da tabela {table} com o ID {ID}",
     *     operationId="deletePet",
     *     tags={"Default"},
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, grupoprestador, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadorfavoritocliente, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="cupompessoa"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID a ser deletado",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=42
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Registro excluído com sucesso."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Registro {ID} não encontrado",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Registro não excluído - Erro de validação",
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/default/{table}",
     *     summary="Retorna TODOS os registros da tabela {table}",
     *     tags={"Default"},
     *     description="Retorna TODOS os registros da tabela {table}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadorfavoritocliente, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="pessoas"
     *         )
     *     ),
     * )
    */
    
     /**
     * @OA\Get(
     *     path="/api/v1/default/{table}/{ID}",
     *     summary="Retorna o registro com id {ID} da tabela {table}",
     *     tags={"Default"},
     *     description="Retorna o registro especificado por {ID}, na tabela {table}. Para dados detalhados do PEDIDO, utilize a rota /api/v1/pedido/{ID}.",
     *     @OA\Response(
     *          response=200,
     *          description="Registro encontrado!",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Registro não encontrado!",
     *     ),
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="cupom"
     *         )
     *     ),      
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID a ser buscado na tabela {table}. Geralmente é o próprio campo ID, menos para as tabelas ATIVIDADE e GRUPO (cujos ID's se chamam ATIVIDADE varchar(4), e GRUPO varchar(15))",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="1"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/lince_hilab_webhook",
     *     summary="Adiciona uma nova linha à tabela lince_hilab_webhook",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da lince_hilab_webhook",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Parameter(
     *         name="parceiro_id",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="d4521900-e2f0-11ea-bb5c-737279be7464"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="token",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="9TQhxZdadIk4a9qNaj12K0NzACS1B11oAgTlx/9PChinJcO/MvBCK9A1ZDa8JxXRXxOQ6KKS49IF/J+s5Uoflx0EcVms1XKwNQgWJhYXSXPfIl7V5sCPhL+JieK5n1R2Qn2i3ZryNJ1jJ00GU6lfCZbUXLkVGR1srn+yc23acCx7voz3xq3Pl1wqAXIXp6ZCdnRaxS6Z8zMAxj1v51Ggu0STo+dnIBcvTHdD9WkOQa2/+vS5188I0ZOQZD8DSkBm/zWjVZ6ryZf6dQsPYWFSPg98cqLDkVXf9acQB2ev8+4="
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="exam_start"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="timestamp",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1597415129672
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="start_time",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1597415129672
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="patient",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="{}"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CNPJ",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="123123123"
     *         )
     *     )
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/lince_hilab_webhook/{ID}",
     *     summary="Edita a linha especificada da tabela lince_hilab_webhook",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da lince_hilab_webhook",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="bfec2a20-e627-11ea-8d7f-fbca7fe3232d"
     *         )
     *     ),
    *     @OA\Parameter(
     *         name="parceiro_id",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="d4521900-e2f0-11ea-bb5c-737279be7464"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="token",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="9TQhxZdadIk4a9qNaj12K0NzACS1B11oAgTlx/9PChinJcO/MvBCK9A1ZDa8JxXRXxOQ6KKS49IF/J+s5Uoflx0EcVms1XKwNQgWJhYXSXPfIl7V5sCPhL+JieK5n1R2Qn2i3ZryNJ1jJ00GU6lfCZbUXLkVGR1srn+yc23acCx7voz3xq3Pl1wqAXIXp6ZCdnRaxS6Z8zMAxj1v51Ggu0STo+dnIBcvTHdD9WkOQa2/+vS5188I0ZOQZD8DSkBm/zWjVZ6ryZf6dQsPYWFSPg98cqLDkVXf9acQB2ev8+4="
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="exam_start"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="timestamp",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1597415129672
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="start_time",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1597415129672
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="patient",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="{}"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CNPJ",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="123123123"
     *         )
     *     )
     * )
     */
    
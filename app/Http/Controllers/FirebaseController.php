<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;

class FirebaseController extends Controller{
   
    function sendPushNotification(Request $request, $token = '') {
                        
        $payload = $request->all();
        $notification = $payload['notification'];
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = "AAAAvAYebcM:APA91bGRiWMVsw2oObI4_8dkS_Vss-PtXZwyktFYRh1ONbq2pMMP2_jdb-FQBErULvEw7z_MVm6hYmsvbe2eOGGkbSxuABfiwufJxQ_M6UfGBYvDiThVSRSHrGtxGcTm6TSZUVfZv7z6";

		// token celular
        // $token = "etRacpWaBiU:APA91bGz5eGLghAixgg1wniWRL1jb27-ScxUWkOWjSaPXoCa9LAI91Qu4X-GfI0M_5pHQOtqBIlqe_r7C1g5ZkCFuGmyOahkN-51bpFjgBBli60HRC4hM_v0XvefsZrbryIC8CDAQ23p";
        
        // token firebase
        // $serverKey = "AIzaSyC1jS1TEVSsC8ZV48ruVY1GHc4dLM8Apy4";
        // $serverKey = "AIzaSyDfDvCqXtz2qHpjO0UL2cBqRQe1gxFLSMA";
		
        $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
		
		$json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, $url);

        //Send the request
        $response = curl_exec($ch);
        //curl_exec($ch);
            
        //Close request
        if ($response === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
        }
        
        curl_close($ch);

    }
}

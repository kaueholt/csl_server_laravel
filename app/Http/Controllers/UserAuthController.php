<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailRulesController;
use App\Http\Controllers\Utils\Handles;
use App\Models\pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserAuthController extends Controller
{
    private $Usuario;

    /**
     * UserAuthController constructor.
     */
    public function __construct()
    {
    }

    /**
     * Search pessoa with cnpjCpf and password.
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $password
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        $payload = $request->all();
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['TIPOPESSOA'] = null;
        if(!isset($payload['EMAIL']) || !isset($payload['SENHA'])){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário / Senha não informados.'
                )
            );
        }
        $user = pessoa::select('ID', 'NOME', 'NOMEREDUZIDO', 'TIPOPESSOA')
            ->where('EMAIL', $payload['EMAIL'])
            ->where('SENHA', $payload['SENHA'])
            ->whereNotNull('SENHA')
            ->where('TIPOPESSOA','C')
            ->first();
        if ($user){
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        $isPrestador = pessoa::select('ID', 'NOME', 'NOMEREDUZIDO', 'TIPOPESSOA')
            ->where("EMAIL", $payload['EMAIL'])
            ->where("TIPOPESSOA",'P')
            ->first();
        if($isPrestador){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário do tipo Prestador'
                )
            );
        }
        $existeEmail = pessoa::select('ID')
            ->where("EMAIL", $payload['EMAIL'])
            ->first();
        if(!$existeEmail){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'E-mail não cadastrado'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Senha inválida'
            )
        );        
    }

    public function loginPrestador(Request $request){
        $payload = $request->all();
        $emptyData['ID'] = null;
        $emptyData['NOME'] = null;
        $emptyData['NOMEREDUZIDO'] = null;
        $emptyData['TIPOPESSOA'] = null;
        if(!isset($payload['EMAIL']) || !isset($payload['SENHA'])){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário / Senha não informados.'
                )
            );
        }
        $user = pessoa::select('ID', 'NOME', 'NOMEREDUZIDO', 'TIPOPESSOA')
            ->where("EMAIL", $payload['EMAIL'])
            ->where("SENHA", $payload['SENHA'])
            ->whereNotNull("SENHA")
            ->where("TIPOPESSOA",'P')
            ->first();
        if ($user){
            return response()->json(
                array(
                    'data' => $user,
                    'code' => 200,
                    'success' => true,
                    'message' => 'Bem vindo!'
                )
            );
        }
        $isCliente = pessoa::select('ID', 'NOME', 'NOMEREDUZIDO', 'TIPOPESSOA')
            ->where("EMAIL", $payload['EMAIL'])
            ->where("TIPOPESSOA",'C')
            ->first();
        if($isCliente){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'Usuário do tipo Cliente'
                )
            );
        }
        $existeEmail = pessoa::select('ID')
            ->where("EMAIL", $payload['EMAIL'])
            ->first();
        if(!$existeEmail){
            return response()->json(
                array(
                    'data' => $emptyData,
                    'code' => 500,
                    'success' => false,
                    'message' => 'E-mail não cadastrado'
                )
            );
        }
        return response()->json(
            array(
                'data' => $emptyData,
                'code' => 500,
                'success' => false,
                'message' => 'Senha inválida'
            )
        );        
    }

    /**
     * Search pessoa with cnpjCpf and password and format return according to admin area
     *
     * @param  varchar $cnpjCpf
     * @param  varchar $password
     * @return \Illuminate\Http\Response
     */
    public function loginAdmin(Request $request)
    {
        // Busca pela usuario que tenha email e senha iguais 
        $user = $this->login($request);

        // verifica resultado 
        if (!empty($user)) {
            return  response()->json(
                array(
                    'code' => 20000,
                    'data' => array('token' => 'admin-token')
                )
            );
        }
        else {
            return  response()->json(
                array(
                    'code' => 60204,
                    'message' => 'Account and password are incorrect.'                )
            );

        }

        // retornar somete um resultado
    }


    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $mail)
    {
        //print_r($mail);
        $pessoa = $this->Usuario->findByEmail($mail[0]);
        $mailer = new MailRulesController();
        
        $sendMail = $mailer->sendMailResetPassword($pessoa[0]->EMAIL, $pessoa[0]->ID);
        //print_r($sendMail);
        return "Status envio: {$sendMail}";

    }

    /**
     * @param Request $mail
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $payload)
    {
        //print_r($payload);

        $sql = "update usuario ";
        $sql .= "set password = '".$payload->password."'";
        $sql .= " where id = ". $payload->userId;

        print_r($sql);

        $affected = DB::update($sql);

        return $affected;

        // // Buscar pela usuario com o id recebido
        // $FINCLIENTE = Pessoa::find($payload->userId);
        // // Insere nova senha
        // $FINCLIENTE->password = $payload->password;
        // // salva os dados, retornando 1 para sucesso e 0 para erro
        // return $FINCLIENTE->save() ? 1 : 0;
    }
    /**
     * info user with id.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function infoUser(Request $request)
    {
        $payload = $request->all();
        isset($payload['CELULAR']) ? $payload['CELULAR'] : isset($payload['celular']) ? $payload['CELULAR'] = $payload['celular'] : null;
        $response = pessoa::select('*')
            ->where("CELULAR", $payload['CELULAR'])
            ->first();
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200) :
                    Handles::jsonResponse(true, 'Nenhum registro encontrado!', [], 404);
    }

}
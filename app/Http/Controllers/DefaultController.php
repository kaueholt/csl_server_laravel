<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\PrestadorController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\PagamentoController;

class DefaultController extends Controller{
    public function destroy($table, $id){
        $model = "App\\Models\\".$table;
        try {
            $query = new $model();
            $response = $model::find($id);
            if(!$response) {
                return Handles::jsonResponse(false, 'Registro não encontrado.', $response, 404);
            }
            $deleteResponse = $response->delete();
            return Handles::jsonResponse(true, 'Registro excluído com sucesso!', $deleteResponse, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse(false, 'Registro não excluído da tabela de endereços. Erro de validação.', $exception, 405);
        }
    }

    public function index(Request $request, $table){
        $model = "App\\Models\\".$table;
        switch($table){
            default:
                $response = $model::all();
            break;
        }            
        if($response){
            unset($response['PASSWORD']);
            unset($response['SENHA']);
            unset($response['EMAILSENHA']);
            unset($response['FONE']);
            unset($response['CELULAR']);
        }
        return Handles::jsonResponse(true, 'Registros encontrados!', $response, 200);
    }

    public function show($table, $id){        
        $model = "App\\Models\\".$table;
        $response = $model::find($id);
        if(!$response) {
            return Handles::jsonResponse(false, 'Registro '.$id.' não encontrado!', $response, 404);
        }
        unset($response['PASSWORD']);
        unset($response['SENHA']);
        unset($response['EMAILSENHA']);
        unset($response['FONE']);
        unset($response['CELULAR']);
        return Handles::jsonResponse(true, 'Registro '.$id.' encontrado!', $response, 200);
    }

    // public function showUid($uid){        
    //     $model = "App\\Models\\usuario";
    //     $response = $model::where('uid', $uid)->first();
    //     if(!$response) {
    //         return Handles::jsonResponse(false, 'UID '.$uid.' não encontrado!', $response, 404);
    //     }
    //     unset($response['PASSWORD']);
    //     unset($response['SENHA']);
    //     unset($response['EMAILSENHA']);
    //     unset($response['FONE']);
    //     unset($response['CELULAR']);
    //     return Handles::jsonResponse(true, 'UID '.$uid.' encontrado!', $response, 200);
    // }

    // public function showCpf($cpf){        
    //     $model = "App\\Models\\usuario";
    //     $response = $model::where('cpf', $cpf)->first();
    //     if(!$response) {
    //         return Handles::jsonResponse(false, 'CPF '.$cpf.' não encontrado!', $response, 404);
    //     }
    //     unset($response['PASSWORD']);
    //     unset($response['SENHA']);
    //     unset($response['EMAILSENHA']);
    //     unset($response['FONE']);
    //     unset($response['CELULAR']);
    //     return Handles::jsonResponse(true, 'CPF '.$cpf.' encontrado!', $response, 200);
    // }
    
    public function store(Request $request, $table){
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['CREATED_AT'] = date('Y-m-d h:i:s');
        DB::beginTransaction();
        try {
            $query = new $model();
            //verifica se nos parâmetros está sendo passada a chave primária, se estiver, verifica se já não existe chave igual
            $chavePrimariaJaExiste = isset($payload[$query->primaryKey]) ? $model::find($payload[$query->primaryKey]) : null;
            if ($chavePrimariaJaExiste){
                return Handles::jsonResponse(false, 'Registro não inserido - Chave '. $query->primaryKey .'->\''.$payload[$query->primaryKey] .'\' já existe!', $chavePrimariaJaExiste, 421);
            }
            switch($table){
                default:
                    $query->fill($payload);
                    $query->save();
                    DB::commit();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $query, 200);
                break;
            }
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            return Handles::jsonResponse(false, $exception->errorInfo[2], [], 405);
        } catch (\InvalidArgumentException $exception){
            DB::rollback();
            return Handles::jsonResponse(false, 'Dados inválidos, verifique os campos '.$exception->getMessage(), [], 422);
        } catch (\Throwable $exception){
            DB::rollback();
            return Handles::jsonResponse(false, 'Erro. '.$exception->getMessage(), [], $exception->getCode() ? $exception->getCode() : 402);
        }
    }
    
    public function update(Request $request, $table, $id){
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['UPDATED_AT'] = date('Y-m-d h:i:s');
        try {
            $response = $model::find($id);
            if(!$response) {
                return Handles::jsonResponse(false, 'Registro não encontrado!', $response, 404);
            }
            switch($table){
                default:
                    $response->fill($payload);
                    $response->save();
                    return Handles::jsonResponse(true, 'Registro '.$response->getKey().' atualizado!', $response, 200);
                break;
            }            
        } catch (\Illuminate\Database\QueryException $exception) {
            // You can check get the details of the error using `errorInfo`:
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, $exception->errorInfo[2], [], 405);
        } catch (\InvalidArgumentException $exception){
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, 'Dados inválidos, verifique os campos.', $exception->getMessage(), 422);
        } catch (\GuzzleHttp\Exception\ClientException $exception){
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, 'Erro na construção da URL da API do Google Maps.', $exception->getMessage(), 425);
        }
    }
}

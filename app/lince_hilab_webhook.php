<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Uuids;

// Column    |          Type          | Collation | Nullable | Default
// -------------+------------------------+-----------+----------+---------
//  id          | uuid                   |           | not null |
//  parceiro_id | uuid                   |           | not null |
//  token       | text                   |           |          |
//  type        | character varying(80)  |           |          |
//  timestamp   | bigint                 |           |          |
//  patient     | json                   |           |          |
//  start_time  | bigint                 |           |          |
//  CNPJ        | character varying(20)  |           |          |
//  custom      | character varying(255) |           |          |

/**
 * Class lince_hilab_webhook
 *
 * @property string $id
 * @property string $parceiro_id
 * @property string $token
 * @property string $type
 * @property int $timestamp
 * @property string $patient
 * @property int $start_time
 * @property string $CNPJ
 * @property string $custom
 *
 * @package App\Models
 */


class lince_hilab_webhook extends Model
{
    protected $table = 'lince_hilab_webhook';
	public $primaryKey = 'id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'timestamp' => 'int',
		'start_time' => 'int'
	];

	protected $fillable = [
		'parceiro_id',
		'token',
		'type',
		'timestamp',
		'patient',
		'start_time',
		'CNPJ',
		'custom'
	];
}


// {
// 	"token": "9TQhxZdadIk4a9qNaj12K0NzACS1B11oAgTlx/9PChinJcO/MvBCK9A1ZDa8JxXRXxOQ6KKS49IF/J+s5Uoflx0EcVms1XKwNQgWJhYXSXPfIl7V5sCPhL+JieK5n1R2Qn2i3ZryNJ1jJ00GU6lfCZbUXLkVGR1srn+yc23acCx7voz3xq3Pl1wqAXIXp6ZCdnRaxS6Z8zMAxj1v51Ggu0STo+dnIBcvTHdD9WkOQa2/+vS5188I0ZOQZD8DSkBm/zWjVZ6ryZf6dQsPYWFSPg98cqLDkVXf9acQB2ev8+4=",
// 	"type": "exam_start",
// 	"timestamp": 1598281200084,
// 	"patient": "{\"document\": \"07443002904\",\"name\": \"Thaina Caldeiras Teste\",\"email\": \"teste@teste.com\",\"sex\": \"female\",\"birthday\": \"31/07/1996\",\"address\": {\"zipcode\": \"81020620\",\"number\": \"123\",\"city\": \"Curitiba\",\"street\": \"Rua Waldemiro Bley\",\"district\": \"Capão Raso\",\"state\": \"PR\",\"complement\": \"\"},\"phone\": \"41999999999\",\"department\": \"policiacivil\"}",
// 	"parceiroId": "d4521900-e2f0-11ea-bb5c-737279be7464",
// 	"startTime": 1597415129672,
// 	"CNPJ": "22886247000190",
//   "custom": "{\"campo_customizado_qualquer\": \"valor\"}"
//   }


// cada objeto do array é um tipo de webhook result para cada tipo de exame

// {
//   "exemplos": [
//     {
//       "token": "33uTD3xXDDEOOSJ0sdVn2uClzOvzqLdi3hQvNpVBtHwHLDS1prrUGgwRUe/vuyHeOuYEkli8izYACznRwQYjBRePvlhUQo7IEHJq1OMrK4EFVgk5EzDrqtYBkxIl/0oIEYF+8D8Y34yODcb29AOde1aGggup4AZz8nHc42camcxD4mCKmNTvUjUVjK+CmzqOrr6SEWIAzd7e2kNEWvxoBxiwhSq1+cd/6vROl4vaV4kcyxhHYqgWF6eau182ayRyrrsD1DlVd8eBbyyaBXwfQ+ktC8UvMI3l7K0vezb00FI=",
//       "type": "exam_finished",
//       "timestamp": 1598371234170,
//       "result": [
//         {
//           "value": -1,
//           "name": "bHCG",
//           "index": "1",
//           "limits": [
//             {
//               "superior": "9.99999999999",
//               "color": 15051436,
//               "inferior": "0.0",
//               "description": "0 a 10",
//               "label": "Entre 0-10"
//             },
//             {
//               "superior": "1499.999",
//               "color": 10668514,
//               "inferior": "10.0",
//               "description": "10 a 1500",
//               "label": "Entre 10-1500"
//             },
//             {
//               "superior": "999990.0",
//               "color": 11000262,
//               "inferior": "1500.0",
//               "description": "1500",
//               "label": "Superior a 1500"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "MubmJd5qn18ZXGW6pqgEPK0xNVCWCavuwRWXD/ZksND8p1oOnjiuuSZ4ZHaoER0oADX3i+MGFmFdqxcn2gjZ3CMwcSkTeZq32qAGWL19YlgNasQTXtHnvNHYnMmRBYlo5J5pPVn04XmZj7QgNV5Z06T86/F3eOnaJL23SJTxnfGj6XJ2F1qDlITnHcH7DKe1p4x+NLvh9DQQdj1KULpGO9+vafSVaFcIv7DlGDkeRxa5LNLNEfDKQrm2T8ze0U8kxXMgb+KOZtt/Ik4ETWkZttWgWgaSr+ACE7PKqWeSR9s=",
//       "type": "exam_finished",
//       "timestamp": 1598371255211,
//       "result": [
//         {
//           "value": -1,
//           "name": "Hemoglobina Glicada",
//           "index": "3",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Invalido",
//               "label": "Invalido"
//             },
//             {
//               "superior": 2,
//               "color": 6143186,
//               "inferior": 0,
//               "description": "Inferior a 2%",
//               "label": "Inferior a 2%"
//             },
//             {
//               "superior": 10,
//               "color": 6143186,
//               "inferior": 2.1,
//               "description": "Entre 2% e 10%",
//               "label": "Entre 2% e 10%"
//             },
//             {
//               "superior": 100,
//               "color": 6143186,
//               "inferior": 10.1,
//               "description": "Acima de 10%",
//               "label": "Acima de 10%"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "pDNiR8N/HRI/bcUxuuYFpLHcmGf+xZLq+hsApegnxhUQpysffg3Ox4d1ryYg2l0pIkGScL+pw8BB9UxE3gZ29Udh51WXAJaDVcgrOxqN+nl7q7GL9VX+l5xJr9HFNRNYAXgbga9GfgNeQa8Yxb5HuTW6QieMy74ISW3n+ADGkRh2mryg2ApXpGpSCCRpNop5xP5dq4uBPJr0gYyR/MaBYFcxNouz65Nri2cQYu27h3tvM01dAjUDi4qGeMcw1hvCD4sjqslNJdNHvOppwAumavd0/TrjDz7nmh4fOI1TAPE=",
//       "type": "exam_finished",
//       "timestamp": 1598371284059,
//       "result": [
//         {
//           "value": 1,
//           "name": "HCV",
//           "index": "4",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 12632256,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": "0.49",
//               "color": 15051442,
//               "inferior": "0",
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": "Infinity",
//               "color": 10999494,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "bi6JyXPREcmFZ49ogBSHL/mMnImNboKHibCy5gI9AuplmcIrkWUjL9YIrobvyGhdv9tZTWTrfJte7RkM8OvCoVyjYPaGBrxVOcW5Lql1xFqH8qYN5XvWg9fRPztr6nKTECRef8Ahu1T1lDtLg7Qk1+tIf6nvKnC0ve2J3DeDvj6J0//k0mE1/E85LaUrrng6O78dwBryFZvKiync/GsZnq+VgneMRm/M6V5UoXYHlWIsejgq1SAeATNhaRkahWRltkEcayBz08zBWc/iBimho7+BMjVPkUYC4SNMeovRpN8=",
//       "type": "exam_finished",
//       "timestamp": 1598371301166,
//       "result": [
//         {
//           "value": 0,
//           "name": "TSH",
//           "index": "5",
//           "limits": [
//             {
//               "superior": "4.2",
//               "inferior": "0.0",
//               "description": "0 a 4.2",
//               "label": "Entre 0-4.2"
//             },
//             {
//               "superior": "20.0",
//               "inferior": "4.20001",
//               "description": "4.2 a 20",
//               "label": "Entre 4.2-20"
//             },
//             {
//               "superior": "999999.0",
//               "inferior": "20.00000001",
//               "description": "20",
//               "label": "Superior a 20"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "HoCA9cGKB00Se9GxDRLxMWhg7JLrmQnOZeQp37ryUxNRj+ir1wJoNF11TnVybY3oMOIRyWnHG0iRiGtkIwVKnDvg3qi9LXxRXcdjfLnkZhkkRHIxTA5zwrUJGmv4F41zQBA2lzdlt9zeRZ5cVEXT9v3JuDR/3IyU5spPu3YeoKDLQixawaTyAhFbNBhuHrl00181Di93NfiS/x5Agzc98SlETlkv480J78grHafGmSfXG3X4B9a0cU1Ek4Xljw2qfbslDKeJbAD6HiitDN+8cirNp5UbrO7V+ukbEx+QjD8=",
//       "type": "exam_finished",
//       "timestamp": 1598371315634,
//       "result": [
//         {
//           "value": 0,
//           "name": "Dengue IgG",
//           "index": 61,
//           "limits": [
//             {
//               "superior": 0.4999999,
//               "inferior": 0,
//               "description": "Nao Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         },
//         {
//           "value": 0,
//           "name": "Dengue IgM",
//           "index": 62,
//           "limits": [
//             {
//               "superior": 0.4999999,
//               "inferior": 0,
//               "description": "Nao Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "IjXve9e02zWS202SoDYvXDzBbjFq46RFBPeEMn63bYzIHT0tWehwcJmw9pgWNpByq3PXOUlauwF758sdNV9gY+I2ru/rDQPegHRXWPnJDbA1+VIIWnHpMlQ2fTGVu1vhHanduikQiV94+kJtgHEJ8rCeJ7O7kaK58MWNXAQy6lvjWi+IGrFXVgYZic2bUKlZEuSYok1+ou5884OIHE218E9rN65l8kENnvnVbUTSf0xWjGQes8lto7H4XNWmvaFy4FfsHki3tPXoA2YqnLltf7hQ95lvrLY8N6NVE4JdCck=",
//       "type": "exam_finished",
//       "timestamp": 1598371325118,
//       "result": [
//         {
//           "value": -1,
//           "name": "PSA",
//           "index": "7",
//           "limits": [
//             {
//               "superior": "-0.01",
//               "color": 15051436,
//               "inferior": "-2.0",
//               "description": "EXAME INV�?LIDO",
//               "label": "EXAME INV�?LIDO"
//             },
//             {
//               "superior": "3.9",
//               "color": 15051436,
//               "inferior": "0.0",
//               "description": "Inferior a 4 ng/ml",
//               "label": "Inferior a 4 ng/ml"
//             },
//             {
//               "superior": "999999.0",
//               "color": 15051436,
//               "inferior": "4.0",
//               "description": "Superior a 4 ng/ml",
//               "label": "Superior a 4 ng/ml"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "bi6JyXPREcmFZ49ogBSHL/mMnImNboKHibCy5gI9AuplmcIrkWUjL9YIrobvyGhdv9tZTWTrfJte7RkM8OvCoVeyAdZNzO7RP0BLIZlAc4Wf+gzWEyDW/CL4HqNj0Nv3B8SSxXbdynmlv9xtoiDO9iGq0N2xkGvrKOxPKczb7g/f/eu/oSvE/eLHYaVlGhFDmkLWuWX02w3igvHYM5X6z+GyZphcAtixEXd+ejUevZb1LrWCYOGcC+L9WAyTkMzUDscuIJov9lopCbrkc6awZxN3QTuWvwTie5/yfGLC3MQ=",
//       "type": "exam_finished",
//       "timestamp": 1598371338660,
//       "result": [
//         {
//           "value": 54.092,
//           "name": "Vitamina D",
//           "index": "8",
//           "limits": [
//             {
//               "superior": 9.9999999,
//               "inferior": 0,
//               "description": ">10"
//             },
//             {
//               "superior": 19.99999999,
//               "inferior": 10,
//               "description": "10 a 20"
//             },
//             {
//               "superior": 29.999999,
//               "inferior": 20,
//               "description": "20 a 30"
//             },
//             {
//               "superior": 99999999,
//               "inferior": 30,
//               "description": "<30"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "uczkQlaJ5HENIrEXY8Mxw63UhalRLU6HIyD7sZWyL9lK/RkNQ1ghRmGpNe2WHmsU+xTq414sdLLy2p0YbrHdke3XmFvBllAZpfBnll4zztjBi46yJaDuDqShamCm55VI/gFywdEZrn/7xP+FDYoCp37HuoIGb1oADFMl8n6JIB710rP5MkmCBzsqrcjPVAOo0u9VbIJBbodX5ss73Ntfyb6ccybw2tSYxV9wMTWyLfqeLF7qIw5QpQn+TehnQd8rg7yKvP1sReZ7OrKOuvd9PXwVeyqNgVxW17rpf43gcJE=",
//       "type": "exam_finished",
//       "timestamp": 1598371348076,
//       "result": [
//         {
//           "value": 0,
//           "name": "HIV",
//           "index": "9",
//           "limits": [
//             {
//               "superior": "0.49999999",
//               "color": 213329,
//               "inferior": "0.0",
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": "9.9999999",
//               "color": 213329,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "xfYXHpJPmddyaVM6xH+aBUL5rfAZJaPKmBj4RfDP1ojMat36M8ba3vM+hEbG7ohDs6+6igDCQjmYapBObIFOhCoeStsXDn1yRAZZJjgGmdiy+VQbDKB7fgiNOqSKMOiyuUCjdrAl1Dm+dE9FGGWw1YF7bwZ+UNlW+ki6Hc77rf7ckRJsFQHeYvoUqP+ia/dj+CoeSDu1NRY67H89RFPuC12AEvLj2cAIt7s+09soBI6TIbZcJE5wtzHBnq3kYOKxzMseTjIRXhcW0hU8BdMgpUxnTaFBKLKm2lyWVgqy47s=",
//       "type": "exam_finished",
//       "timestamp": 1598371360357,
//       "result": [
//         {
//           "value": -1,
//           "name": "CHOL",
//           "index": 101,
//           "limits": [
//             {
//               "superior": "99.99",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "Abaixo de 100",
//               "label": "Abaixo de 100"
//             },
//             {
//               "superior": "400.0",
//               "color": 10934726,
//               "inferior": "100.0",
//               "description": "Entre 100 e 400",
//               "label": "Entre 100 e 400"
//             },
//             {
//               "superior": "Infinity",
//               "color": 10934726,
//               "inferior": "400.1",
//               "description": "Superior a 400",
//               "label": "Superior a 400"
//             }
//           ]
//         },
//         {
//           "value": -1,
//           "name": "HDL",
//           "index": 102,
//           "limits": [
//             {
//               "superior": "19.99999",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "Abaixo de 20",
//               "label": "Abaixo de 20"
//             },
//             {
//               "superior": "100.0",
//               "color": 10934726,
//               "inferior": "20.0",
//               "description": "Entre 20 e 100",
//               "label": "Entre 20 e 100"
//             },
//             {
//               "superior": "Infinity",
//               "color": 10934726,
//               "inferior": "100.01",
//               "description": "Superior a 100",
//               "label": "Superior a 100"
//             }
//           ]
//         },
//         {
//           "value": -1,
//           "name": "TG",
//           "index": 103,
//           "limits": [
//             {
//               "superior": "99.99999",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "Abaixo de 100",
//               "label": "Abaixo de 120"
//             },
//             {
//               "superior": "400.0",
//               "color": 10934726,
//               "inferior": "100.0",
//               "description": "Entre 100 e 400",
//               "label": "Entre 100 e 400"
//             },
//             {
//               "superior": "Infinity",
//               "color": 10934726,
//               "inferior": "400.1",
//               "description": "Superior a 400",
//               "label": "Superior a 400"
//             }
//           ]
//         },
//         {
//           "value": -1,
//           "name": "NHDL",
//           "index": 104,
//           "limits": [
//             {
//               "superior": "380.0",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "desc",
//               "label": "eqw"
//             }
//           ]
//         },
//         {
//           "value": -1,
//           "name": "LDL",
//           "index": 105,
//           "limits": [
//             {
//               "superior": "370.0",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "descricao",
//               "label": "label"
//             }
//           ]
//         },
//         {
//           "value": -1,
//           "name": "VLDL",
//           "index": 106,
//           "limits": [
//             {
//               "superior": "1.0E21",
//               "color": 10934726,
//               "inferior": "0.0",
//               "description": "desc",
//               "label": "qwe"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "pDNiR8N/HRI/bcUxuuYFpLHcmGf+xZLq+hsApegnxhXXcqOPURQPqaZEBN4qPlmuGNyd3FEhEqSYrP2I+U0BdSFihJsF5cHITEbUEcZMSiLZrFyXxWM88y0USbxjOHBrneSCjngS8wEVxVI55buL4nzk+3FkUaF63StJRs38uNsUwYX3i1Dml+fwt7xTEC2ij0HqAkXsrk2A0zeytHJf2i88kmY+yUPCJyHIQYCU854jwcOOz/VIdYtZlBhY2voUtkl/vgR2BsTcMHo6P/MzOmaOdDxXTgZxRZNVENqIaqQ=",
//       "type": "exam_finished",
//       "timestamp": 1598371386967,
//       "result": [
//         {
//           "value": 0,
//           "name": "Dengue NS1",
//           "index": "11",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Invalido",
//               "label": "Invalido"
//             },
//             {
//               "superior": 0.4999999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": 9999999,
//               "color": 213329,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "nF6L1cabJMS5FqvY6G/ShlfXVeFF8TUFxVikbk67boeciIOcGT/FtaeKrx9rXyh9qN2J1r2fyJauitXYdrk9PyZOPhHRjDCkcVONDMDkIOObK4+fEg1QYP+mH9eIj25xbCOb+0dlw1Vq1kVUrrBQVasyVhhO3SWVN/ooTMaUnNuvbkOd3uiqYtITNtueCy+Ba6+wkyUKsRbdw84/PbeCx0ZCJxy7hKtUbFI/iOkE+ApuDMiWwmMG//8oZYpXIHI8867GX+85sN/Wbd4J3b0gbF9DxZx073OtBd0oFJvoQHI=",
//       "type": "exam_finished",
//       "timestamp": 1598371400797,
//       "result": [
//         {
//           "value": 0,
//           "name": "Sífilis",
//           "index": "12",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": "0.4999999999",
//               "color": 213329,
//               "inferior": "0.0",
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": "Infinity",
//               "color": 213329,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "pDNiR8N/HRI/bcUxuuYFpLHcmGf+xZLq+hsApegnxhUQpysffg3Ox4d1ryYg2l0pIkGScL+pw8BB9UxE3gZ29W0jKBPw87iKQgWDH/YZd1kv+92cIUXvvtmvqry4SkJzzvkz/D/3dRGccG0H5VBA325JVCSQhv6k9HkVx6JXHMlTxOtjI2G2h/95YV0ev6rWishTrGcofopbkmgWT744QwQaWFVRjQ8A/gl7rwv0OUilGKRihnEycORD943j6HoyeNZfLlFns3Ng+1rleehvxvnt7Ni0M8UEKhKFfyzw7bk=",
//       "type": "exam_finished",
//       "timestamp": 1598371410628,
//       "result": [
//         {
//           "value": 0,
//           "name": "HBsAg",
//           "index": "13",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": "0.4999999999",
//               "color": 213329,
//               "inferior": "0.0",
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": "Infinity",
//               "color": 213329,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "pDNiR8N/HRI/bcUxuuYFpLHcmGf+xZLq+hsApegnxhUQpysffg3Ox4d1ryYg2l0pIkGScL+pw8BB9UxE3gZ29Rit++S5PzK9RA05/ImwRd9Yet+FoQ9wFsD6Vz4iqp0K+bEaUFSzypVCWn5Yb0eAjMYxEPrmbqYeccdNx4VBmjpJtxEo3S8TuwLg/M0H6Dud0yemkuINsJTGRbUlRxdjeJSb4VaDmGVLzY2f3oG4z57raXccTUep5froq5xRija6wqidnjEcSnlEw8nNiPQytc0secsWZPYLMI7NN4nNCnE=",
//       "type": "exam_finished",
//       "timestamp": 1598371419741,
//       "result": [
//         {
//           "value": 0,
//           "name": "Anti-HBsAg",
//           "index": "14",
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": "0.4999999999",
//               "color": 213329,
//               "inferior": "0.0",
//               "description": "Não Reagente",
//               "label": "Não Reagente"
//             },
//             {
//               "superior": "Infinity",
//               "color": 213329,
//               "inferior": "0.5",
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "CQZktNJwiPuS6OFa2SU9L3iIjrnfIqoTcsO5D8+J84JY+ZBF4e/HbXX3YRSYyBQ0x51yVhleSKSeqEyV1OdAKfT9nCqc8BOkZfhtM0/Vd0bKPrP1YALccx2u/4EtHnme1mAmLBYBUBHHLbRVUzF2TlSDyC3S4RV5M9BlssTgyP4cp2+w0h27xivOfhU/lzn6c39HJujee9gW8LsZGZT6gYW9b4Vnmn/4Qhv3wH905URujp2E0CTeaHUjBERnWQ2d1hHXt+o6pNrMJvEwq+EGK4BX7AV6kYzey0EIfhfLSRY=",
//       "type": "exam_finished",
//       "timestamp": 1598371428774,
//       "result": [
//         {
//           "value": 0,
//           "name": "Zika IgG",
//           "index": 151,
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         },
//         {
//           "value": 0,
//           "name": "Zika IgM",
//           "index": 152,
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "DNVqnLFmAbXNgfr6LzHX0sSgh4lsl5snIralqGaR4Q+1JVDK8RiDb92KxUfNPwDnjat0iBcptOkyUOENeT7XTVj+JwLJZ83tvqGc2Ircj9rwmVrOeQUMNcQ9XahTdd3JNKjrqfHCx9HSwO6Frc50a8hZK/TEbZfskinH/kZRWZVBLji2F3/K32hHEIsyy5xaJr+tQJlBjliIsSZxdfIPuWq4BJinpAL45kqpEZseJJVxeXHRUI+p/T5MD4prsbqK5OJToCr9SiN2j/jIcx62NX7WlHDwnfAJJib/898ejWc=",
//       "type": "exam_finished",
//       "timestamp": 1598371438379,
//       "result": [
//         {
//           "value": 106.078,
//           "name": "Glicemia",
//           "index": "17",
//           "limits": [
//             {
//               "superior": -0.01,
//               "inferior": -2
//             },
//             {
//               "superior": 19.99999999,
//               "inferior": 0
//             },
//             {
//               "superior": 399.99999999,
//               "inferior": 20
//             },
//             {
//               "superior": 99999999,
//               "inferior": 400
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "99lD0DZttxcaX7u3hlfecMGfH9PTy2+Gj+qhIksff0tSYoWoR2iPHlIYG1RCWi+Lw4cYOTWYAbzFKXVhlXITjYzm0fepRXdSo8wSpyywoZB/kMKAKLicIF+x/XvAflOHyNwl1l5Mers1Jwl0IsCx5B7GdWV9FIFy4JMKQCfd/pY53dkg1dm6KsFLsu3c+UMu22DRsdTVyPuAgnk3z/31qfk+2fTnhiKjP9v4eeJS4RJZR82/H9jZGqPtD9nwVybzkQ6rOW6g6LIiXDfLyhdnpayVP+SZNG2LwpMTHXeBIPM=",
//       "type": "exam_finished",
//       "timestamp": 1598371451893,
//       "result": [
//         {
//           "value": 0,
//           "name": "Influenza A",
//           "index": "181",
//           "limits": [
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         },
//         {
//           "value": 0,
//           "name": "Influenza B",
//           "index": "182",
//           "limits": [
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     },
//     {
//       "token": "bmKgyIAMLvwpiHXGIKDeKfBKSU7sA2d46GUWE5XQSZ/yG+jZjox96oCxK9PDBa16MgvTU2Oa5DYqWzeCLPfMQL3YGPCQKiljfGWmrM2Y/b1h697DxXJzXOePNLHA6zQ4goWOBOLIMhSNenZ2kwnKpI9gvDSQsE++zK7WQD8POtoQMg1vkFPeHJSjfNEfDjn3CLadoERfs0NwjxmD1HXZm+YwLRKBgVX/uCWj34zsaCSM3lIsDNp2GUUhPXXADRd/PUYsppHM6F7p7vDepKkVNZneDQXgP3ppsB5+/K/C1xmgzezuwGsSXwnbor88c4hCYgi4UBSKnBtqp5bbPuWn/MR3+bo36YybbR7484EZwkc=",
//       "type": "exam_finished",
//       "timestamp": 1598371461422,
//       "result": [
//         {
//           "value": 0,
//           "name": "COVID-19 IgG",
//           "index": 1901,
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         },
//         {
//           "value": 0,
//           "name": "COVID-19 IgM",
//           "index": 1902,
//           "limits": [
//             {
//               "superior": -0.01,
//               "color": 213329,
//               "inferior": -2,
//               "description": "Inválido",
//               "label": "Inválido"
//             },
//             {
//               "superior": 0.4999999,
//               "color": 213329,
//               "inferior": 0,
//               "description": "Não Reagente",
//               "label": "Nao Reagente"
//             },
//             {
//               "superior": 10000,
//               "color": 213329,
//               "inferior": 0.5,
//               "description": "Reagente",
//               "label": "Reagente"
//             }
//           ]
//         }
//       ],
//       "id": ""
//     }
//   ]
// }
